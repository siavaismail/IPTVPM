$("#save-btn").click(function() {
    var input = document.getElementById('text');
    var fileName = document.getElementById('text-filename');

    var blob = new Blob([input.value], {type: "text/plain;charset=utf-8"});
    saveAs(blob, fileName.value+".txt");
});